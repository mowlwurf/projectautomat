ProjectAutomat
==============

ProjectAutomat is a tool simplifying project creation for sf projects including bootstrap,jquery & grunt,
or just a clean sf project by just one command

# Requirements

- PHP5+
- npm

# Usage

Creating a new Bootstrap Symfony project

```php
app/console create:project:bootstrap <name>
```

Creating a new Vanilla Symfony project

```php
app/console create:project:vanilla <name>
```

After Project is created, and has installed all dependencies, you can generate your first bundles. There are two types of bundles: no-web, web

## web bundle type

This is a default generated sf bundle including DefaultController and routing. config type is yml by default (will be configurable soon!)

## no-web bundle type

This is a light-weight sf bundle, which lost controller, routing and their dependencies

#Commands

## bootstrap

Including: Symfony, Deployer, Bower, Grunt, Bootstrap, jQuery on newest releases

## vanilla

Including: Symfony, Deployer

# Extensions Loadable

After finished project & bundle initialization, you are able to register extension you want to use like Monolog or Doctrine ORM, SQLite etc.
Like the Bundle request you will be asked for adding that much extensions you want to. 

## List of implemented Extensions

- Monolog (incoming)
- Doctrine Connetion/ORM/Migrations (incoming)
- SQLite (incoming)