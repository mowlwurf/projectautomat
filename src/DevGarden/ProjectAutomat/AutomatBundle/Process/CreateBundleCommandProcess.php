<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Process;


class CreateBundleCommandProcess extends BaseProcess
{
    CONST CMD_PATTERN = 'app/console create:bundle %s %s --type %s';

    public function __construct(){
        parent::__construct(self::CMD_PATTERN);
    }

    /**
     * @param string $dir
     * @param string $name
     * @param string $type
     * @param bool $verbose
     * @return bool
     */
    public function execute($dir, $name, $type, $verbose = false){
        $this->setCommandLine(
            sprintf(self::CMD_PATTERN, $name, $dir, $type)
        );
        return $this->executeProcess($verbose);
    }
}