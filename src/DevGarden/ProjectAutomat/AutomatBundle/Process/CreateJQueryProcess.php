<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Process;

class CreateJQueryProcess extends BaseProcess
{
    CONST CMD = 'bower install jquery';

    public function __construct(){
        parent::__construct(self::CMD);
    }

    /**
     * @param $dir
     * @param bool $verbose
     * @return bool
     */
    public function execute($dir, $verbose = false){
        $this->setWorkingDirectory($dir);
        return $this->executeProcess($verbose);
    }
}