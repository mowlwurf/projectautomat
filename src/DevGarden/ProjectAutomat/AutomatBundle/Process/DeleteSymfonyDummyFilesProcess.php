<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Process;


class DeleteSymfonyDummyFilesProcess extends BaseProcess
{
    CONST CMD = 'rm -Rf src/AppBundle';

    public function __construct(){
        parent::__construct(self::CMD);
    }

    /**
     * @param $dir
     * @param bool $verbose
     * @return bool
     */
    public function execute($dir, $verbose = false){
        $this->setWorkingDirectory($dir);
        $this->cleanUpAppKernel();
        $this->cleanUpRouting();
        return $this->executeProcess($verbose);
    }

    protected function cleanUpAppKernel(){
        $file = $this->getWorkingDirectory() . '/app/AppKernel.php';
        $appKernelRaw = file_get_contents($file);
        $appKernelRaw = str_replace(
            '            new AppBundle\AppBundle(),' . PHP_EOL,
            '',
            $appKernelRaw
        );
        file_put_contents($file, $appKernelRaw);
    }

    protected function cleanUpRouting(){
        $file = $this->getWorkingDirectory() . '/app/config/routing.yml';
        file_put_contents($file, '');
    }
}