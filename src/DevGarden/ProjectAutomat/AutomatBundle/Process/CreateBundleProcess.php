<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Process;

class CreateBundleProcess extends BaseProcess
{
    CONST CMD_PATTERN = 'app/console generate:bundle --namespace %s --dir %s --bundle-name %s --format %s';

    public function __construct(){
        parent::__construct(self::CMD_PATTERN);
    }

    /**
     * @param $dir
     * @param $name
     * @param $publisher
     * @param bool $verbose
     * @return bool
     */
    public function execute($dir, $name, $publisher, $verbose = false){
        $pathArray = explode('/', $dir);
        $namespace = sprintf('%s/%s/%s', $publisher, array_pop($pathArray), $name);
        $this->setWorkingDirectory($dir);
        $this->setCommandLine(sprintf(
            $this->getCommandLine(),
            $namespace,
            $dir.'/src/',
            $name,
            'yml'
        ));
        $this->executeProcess($verbose);
        return true;
    }
}