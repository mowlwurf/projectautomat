<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Process;

class DeleteSymfonyDefaultControllerProcess extends BaseProcess
{
    CONST CMD_PATTERN = 'rm -Rf %s';

    public function __construct(){
        parent::__construct(self::CMD_PATTERN);
    }

    /**
     * @param $dir
     * @param bool $verbose
     * @return bool
     */
    public function execute($dir, $verbose = false){
        $this->setWorkingDirectory($dir);
        $this->removeController($verbose);
        $this->removeControllerTests($verbose);
        $this->removeControllerViews($verbose);
        $this->removeBundleRouting($verbose);
        $projectInfo = explode('/', $dir);
        array_pop($projectInfo);
        $this->cleanUpRouting(array_pop($projectInfo));
    }

    /**
     * @param bool $verbose
     */
    protected function removeController($verbose){
        $this->setCommandLine(sprintf(
            $this->getCommandLine(),
            'Controller/'
        ));
        $this->executeProcess($verbose);
    }

    /**
     * @param bool $verbose
     */
    protected function removeControllerTests($verbose){
        $this->setCommandLine(sprintf(
            self::CMD_PATTERN,
            'Tests/'
        ));
        $this->executeProcess($verbose);
    }

    /**
     * @param $verbose
     */
    protected function removeControllerViews($verbose){
        $this->setCommandLine(sprintf(
            self::CMD_PATTERN,
            'Resources/views/'
        ));
        $this->executeProcess($verbose);
    }

    /**
     * @param $verbose
     */
    protected function removeBundleRouting($verbose){
        $this->setCommandLine(sprintf(
            self::CMD_PATTERN,
            'Resources/config/routing.yml'
        ));
        $this->executeProcess($verbose);
    }

    /**
     * @param $bundleName
     */
    protected function cleanUpRouting($bundleName){
        $file = $this->getWorkingDirectory() . '../../../../app/config/routing.yml';
        $routingYmlRaw = file_get_contents($file);
        $routingYmlParsed = explode("\n", $routingYmlRaw);
        $c = 0;
        $newRoutingParsed = array();
        $isFirstEmptyRow = true;
        foreach($routingYmlParsed as $key => $row){
            if ($key == 0 && trim($row) == '') {
                continue;
            }
            if (strpos($row, $this->getConfigKeyByBundleName($bundleName).':') !== false ) {
                // count first of three lines that will be stripped
                $c = 1;
                continue;
            }
            if ($c) {
                $c++;
                if($c <= 3){
                    continue;
                } else {
                    $c = 0;
                }
            }
            if (trim($row) == '') {
                if ($isFirstEmptyRow) {
                    $isFirstEmptyRow = false;
                } else {
                    continue;
                }
            } else {
                $isFirstEmptyRow = true;
            }
            $newRoutingParsed[] = $row;
        }
        file_put_contents($file, implode("\n", $newRoutingParsed));
    }

    /**
     * @param $method
     * @return string
     */
    protected function getConfigKeyByBundleName($method){
        $split = $this->splitAtUpperCase($method);
        if (count($split) > 3) {
            $method = '';
            foreach($split as $str){
                if ($str == 'Bundle' || trim($str) == ''){
                    continue;
                }
                $method .= '_' . strtolower($str);
            }
            $method = ltrim($method, " _");
        } else {
            $method = strtolower($split[1]);
        }
        return $method;
    }

    /**
     * @param $s
     * @return array
     */
    protected function splitAtUpperCase($s) {
        return preg_split('/(?=[A-Z])/', $s, -1, PREG_SPLIT_NO_EMPTY);
    }

}