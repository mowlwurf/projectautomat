<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Process;


class CreateDeployFileProcess
{
    public function __construct(){
    }

    /**
     * @param $dir
     * @param $projectName
     * @param array $config
     * @return bool
     */
    public function execute($dir, $projectName, array $config){
        $deployFileRaw = <<< 'PHP'
<?php

// All Deployer recipes are based on `recipe/common.php`.
require 'vendor/deployer/deployer/recipe/symfony.php';

// Define a server for deployment.
// Let's name it "prod" and use port 22.
server('prod', 'host', 22)
    ->user('%s')
    ->forwardAgent() // You can use identity key, ssh config, or username/password to auth on the server.
    ->stage('production')
    ->env('deploy_path', '/var/deployment/%s'); // Define the base path to deploy your project to.

// Specify the repository from which to download your project's code.
// The server needs to have git installed for this to work.
// If you're not using a forward agent, then the server has to be able to clone
// your project from this repository.
set('repository', 'git@github.com:org/app.git');
PHP;
        file_put_contents(
            $dir . '/deploy.php',
            sprintf(
                $deployFileRaw,
                $config['server_name'],
                $config['server_host'],
                $config['server_port'],
                $config['server_user'],
                $config['user_key'],
                $projectName
            )
        );
    }
}