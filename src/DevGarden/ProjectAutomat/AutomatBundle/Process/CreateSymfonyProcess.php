<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Process;

class CreateSymfonyProcess extends BaseProcess
{
    CONST CMD_PATTERN = '%ssymfony new %s';

    /**
     * @var string
     */
    protected $binPath;

    /**
     * @param string $project_dir
     * @param string $bin_dir
     */
    public function __construct($project_dir, $bin_dir){
        $this->binPath;
        parent::__construct(self::CMD_PATTERN, $project_dir);
    }

    /**
     * @param $name
     * @param bool $verbose
     * @return bool
     */
    public function execute($name, $verbose = false){
        $this->setCommandLine(
            sprintf(
                $this->getCommandLine(),
                $this->binPath,
                $name
            )
        );
        return $this->executeProcess($verbose);
    }
}