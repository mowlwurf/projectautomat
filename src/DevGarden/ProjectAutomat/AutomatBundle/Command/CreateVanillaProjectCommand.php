<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DevGarden\ProjectAutomat\AutomatBundle\Service\ProcessServiceProvider;

class CreateVanillaProjectCommand extends ContainerAwareCommand implements CreateProjectCommandInterface
{
    /**
     * @var ProcessServiceProvider
     */
    protected $processProvider;

    /**
     * @var bool
     */
    protected $verbose;

    public function configure(){
        $this->setName('create:project:vanilla');
        $this->addArgument('name', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool|int|null
     */
    public function execute(InputInterface $input, OutputInterface $output){
        // option verbose is set by default
        $projectName = $input->getArgument('name');
        $projectDir  = $this->getProjectDir() . $projectName;

        $this->verbose          = $input->getOption('verbose');
        $this->processProvider  = $this->getProcessProvider();
        $output->writeln(sprintf('Creating project %s ...', $projectName));
        $this->createSfProject($projectName);
        $output->writeln('Removing AppBundle ...');
        $this->cleanSfProject($projectDir);
        $output->writeln('Installing deploy ...');
        $this->createDeployFile($projectDir, $projectName);
        $this->createProjectExtensions($output, $projectDir);
        $this->callBundleRequest($output, $projectDir);
        $output->writeln('Finished project initialization of '.$projectName);
        return true;
    }

    /**
     * @param $projectDir
     * @param $projectName
     * @return bool
     */
    protected function createDeployFile($projectDir, $projectName){
        $deployProcess = $this->processProvider->getCreateDeployFileProcess();
        return $deployProcess->execute($projectDir, $projectName, [
            'server_name' => $this->getContainer()->getParameter('deploy_server_name'),
            'server_host' => $this->getContainer()->getParameter('deploy_server_host'),
            'server_port' => $this->getContainer()->getParameter('deploy_server_port'),
            'server_user' => $this->getContainer()->getParameter('deploy_server_user'),
            'user_key'    => $this->getContainer()->getParameter('deploy_user_key')
        ]);
    }

    /**
     * @param OutputInterface $output
     * @param string $projectDir
     * @return mixed
     */
    public function createProjectExtensions(OutputInterface $output, $projectDir){}

    /**
     * @param OutputInterface $output
     * @param string $projectDir
     */
    protected function callBundleRequest(OutputInterface $output, $projectDir){
        $bundleRequest = true;
        while ($bundleRequest) {
            $output->write('Add bundle [name, press return for none]:');
            $userInput = $this->waitForUserInput();
            if (!empty(trim($userInput))) {
                $output->write('BundleType [no-web]:');
                $userInputType = $this->waitForUserInput();
                $bundleCommand = $this->processProvider->getCreateBundleCommandProcess();
                $bundleCommand->execute($projectDir, $userInput, $userInputType, $this->verbose);
                $output->writeln(sprintf('Created bundle %s ...', $userInput));
            } else {
                $bundleRequest = false;
            }
        }
    }

    /**
     * @param string $projectName
     * @return bool
     */
    protected function createSfProject($projectName){
        $sfProcess = $this->processProvider->getCreateSymfonyProcess();
        return $sfProcess->execute($projectName, $this->verbose);
    }

    /**
     * @param string $projectDir
     */
    protected function cleanSfProject($projectDir){
        $cleanUpProcess = $this->processProvider->getDeleteSymfonyDummyFilesProcess();
        $cleanUpProcess->execute($projectDir, $this->verbose);
    }

    /**
     * @return string
     */
    protected function getProjectDir(){
        return $this->getContainer()->getParameter('project_dir');
    }

    /**
     * @return ProcessServiceProvider
     */
    protected function getProcessProvider(){
        return $this->getContainer()->get('automat.process.provider');
    }

    protected function waitForUserInput(){
        $fp = fopen('php://stdin','r');
        return str_replace(array("\n","\r","\r\n"),'',fgets($fp));
    }
}