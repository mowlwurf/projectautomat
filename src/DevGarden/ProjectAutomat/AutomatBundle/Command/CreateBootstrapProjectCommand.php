<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateBootstrapProjectCommand extends CreateVanillaProjectCommand implements CreateProjectCommandInterface
{

    public function configure(){
        $this->setName('create:project:bootstrap');
        $this->addArgument('name', InputArgument::REQUIRED);
        $this->addOption('verbose','v',InputOption::VALUE_NONE);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool|int|null
     */
    public function execute(InputInterface $input, OutputInterface $output){
        parent::execute($input, $output);
        return true;
    }

    /**
     * @param OutputInterface $output
     * @param string $projectDir
     * @return mixed|void
     */
    public function createProjectExtensions(OutputInterface $output, $projectDir){
        $output->writeln('Installing bower ...');
        $bowerProcess = $this->processProvider->getCreateBowerProcess();
        $bowerProcess->execute($projectDir, $this->verbose);
        $output->writeln('Installing grunt ...');
        $gruntProcess = $this->processProvider->getCreateGruntProcess();
        $gruntProcess->execute($projectDir, $this->verbose);
        $output->writeln('Installing bootstrap ...');
        $bootstrapProcess = $this->processProvider->getCreateBootstrapProcess();
        $bootstrapProcess->execute($projectDir, $this->verbose);
        $output->writeln('Installing jquery ...');
        $jqueryProcess = $this->processProvider->getCreateJQueryProcess();
        $jqueryProcess->execute($projectDir, $this->verbose);
    }

}