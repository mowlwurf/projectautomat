<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Command;


use DevGarden\ProjectAutomat\AutomatBundle\Service\ProcessServiceProvider;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateBundleCommand extends ContainerAwareCommand
{
    public function configure(){
        $this->setName('create:bundle');
        $this->addArgument('name', InputArgument::REQUIRED);
        $this->addArgument('dir', InputArgument::REQUIRED);
        $this->addOption('type', null, InputOption::VALUE_OPTIONAL,'','no-web');
    }

    public function execute(InputInterface $input, OutputInterface $output){
        $option = $input->getOption('type');
        if (!in_array($option, ['web', 'no-web'])) {
            $output->writeln(sprintf('%s is not a valid bundle type. [\'web\', \'no-web\']', $option));
            return false;
        }
        $projectDir     = $input->getArgument('dir');
        $pathArray      = explode('/', $projectDir);
        $bundleName     = $input->getArgument('name');
        $publisher      = $this->getPublisher();
        $provider       = $this->getProcessProvider();
        $bundleProcess  = $provider->getCreateBundleProcess();
        $bundleProcess->execute($projectDir, $bundleName, $publisher);
        if ($option == 'no-web') {
            $noWebProcess = $provider->getDeleteSymfonyDefaultControllerProcess();
            $noWebProcess->execute(
                sprintf(
                    '%s/src/%s/%s/%s/',
                    $projectDir,
                    $publisher,
                    array_pop($pathArray),
                    $bundleName
                )
            );
        }
    }

    /**
     * @return ProcessServiceProvider
     */
    protected function getProcessProvider(){
        return $this->getContainer()->get('automat.process.provider');
    }

    /**
     * @return string
     */
    protected function getPublisher(){
        return $this->getContainer()->getParameter('project_publisher');
    }
}