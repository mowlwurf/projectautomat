<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

interface CreateProjectCommandInterface
{
    public function configure();

    public function execute(InputInterface $input, OutputInterface $output);

    public function createProjectExtensions(OutputInterface $output, $projectDir);
}