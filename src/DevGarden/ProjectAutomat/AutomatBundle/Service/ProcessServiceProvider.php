<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Service;

use DevGarden\ProjectAutomat\AutomatBundle\Process\CreateBowerProcess;
use DevGarden\ProjectAutomat\AutomatBundle\Process\CreateBundleCommandProcess;
use DevGarden\ProjectAutomat\AutomatBundle\Process\CreateBundleProcess;
use DevGarden\ProjectAutomat\AutomatBundle\Process\CreateDeployFileProcess;
use DevGarden\ProjectAutomat\AutomatBundle\Process\CreateSymfonyProcess;
use DevGarden\ProjectAutomat\AutomatBundle\Process\CreateGruntProcess;
use DevGarden\ProjectAutomat\AutomatBundle\Process\CreateBootstrapProcess;
use DevGarden\ProjectAutomat\AutomatBundle\Process\CreateJQueryProcess;
use DevGarden\ProjectAutomat\AutomatBundle\Process\DeleteSymfonyDefaultControllerProcess;
use DevGarden\ProjectAutomat\AutomatBundle\Process\DeleteSymfonyDummyFilesProcess;

class ProcessServiceProvider
{
    /**
     * @var CreateBowerProcess
     */
    protected $createBowerProcess;

    /**
     * @var CreateSymfonyProcess
     */
    protected $createSymfonyProcess;

    /**
     * @var CreateGruntProcess
     */
    protected $createGruntProcess;

    /**
     * @var CreateBootstrapProcess
     */
    protected $createBootstrapProcess;

    /**
     * @var CreateJQueryProcess
     */
    protected $createJQueryProcess;

    /**
     * @var DeleteSymfonyDummyFilesProcess
     */
    protected $deleteSymfonyDummyFilesProcess;

    /**
     * @var CreateBundleProcess
     */
    protected $createBundleProcess;

    /**
     * @var CreateBundleCommandProcess
     */
    protected $createBundleCommandProcess;

    /**
     * @var DeleteSymfonyDefaultControllerProcess
     */
    protected $deleteSymfonyDefaultControllerProcess;

    /**
     * @var CreateDeployFileProcess
     */
    protected $createDeployFileProcess;

    /**
     * @param CreateSymfonyProcess $sfProcess
     * @param CreateBowerProcess $bowerProcess
     * @param CreateGruntProcess $gruntProcess
     * @param CreateBootstrapProcess $bsProcess
     * @param CreateJQueryProcess $jqProcess
     * @param DeleteSymfonyDummyFilesProcess $rmSfDummyProcess
     * @param DeleteSymfonyDefaultControllerProcess $rmControllerProcess
     * @param CreateBundleProcess $bundleProcess
     * @param CreateBundleCommandProcess $bundleCommand
     * @param CreateDeployFileProcess $deployProcess
     */
    public function __construct(
        CreateSymfonyProcess $sfProcess,
        CreateBowerProcess $bowerProcess,
        CreateGruntProcess $gruntProcess,
        CreateBootstrapProcess $bsProcess,
        CreateJQueryProcess $jqProcess,
        DeleteSymfonyDummyFilesProcess $rmSfDummyProcess,
        DeleteSymfonyDefaultControllerProcess $rmControllerProcess,
        CreateBundleProcess $bundleProcess,
        CreateBundleCommandProcess $bundleCommand,
        CreateDeployFileProcess $deployProcess
    ) {
        $this->createSymfonyProcess             = $sfProcess;
        $this->createBowerProcess               = $bowerProcess;
        $this->createGruntProcess               = $gruntProcess;
        $this->createBootstrapProcess           = $bsProcess;
        $this->createJQueryProcess              = $jqProcess;
        $this->deleteSymfonyDummyFilesProcess   = $rmSfDummyProcess;
        $this->createBundleProcess              = $bundleProcess;
        $this->createBundleCommandProcess       = $bundleCommand;
        $this->deleteSymfonyDefaultControllerProcess = $rmControllerProcess;
        $this->createDeployFileProcess                  = $deployProcess;
    }

    /**
     * @return CreateBowerProcess
     */
    public function getCreateBowerProcess()
    {
        return $this->createBowerProcess;
    }

    /**
     * @return CreateSymfonyProcess
     */
    public function getCreateSymfonyProcess()
    {
        return $this->createSymfonyProcess;
    }

    /**
     * @return CreateGruntProcess
     */
    public function getCreateGruntProcess()
    {
        return $this->createGruntProcess;
    }

    /**
     * @return CreateBootstrapProcess
     */
    public function getCreateBootstrapProcess()
    {
        return $this->createBootstrapProcess;
    }

    /**
     * @return CreateJQueryProcess
     */
    public function getCreateJQueryProcess()
    {
        return $this->createJQueryProcess;
    }

    /**
     * @return DeleteSymfonyDummyFilesProcess
     */
    public function getDeleteSymfonyDummyFilesProcess()
    {
        return $this->deleteSymfonyDummyFilesProcess;
    }

    /**
     * @return CreateBundleProcess
     */
    public function getCreateBundleProcess()
    {
        return $this->createBundleProcess;
    }

    /**
     * @return CreateBundleCommandProcess
     */
    public function getCreateBundleCommandProcess()
    {
        return $this->createBundleCommandProcess;
    }

    /**
     * @return DeleteSymfonyDefaultControllerProcess
     */
    public function getDeleteSymfonyDefaultControllerProcess()
    {
        return $this->deleteSymfonyDefaultControllerProcess;
    }

    /**
     * @return CreateDeployFileProcess
     */
    public function getCreateDeployFileProcess()
    {
        return $this->createDeployFileProcess;
    }

}