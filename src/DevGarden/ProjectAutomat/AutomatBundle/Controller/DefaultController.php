<?php

namespace DevGarden\ProjectAutomat\AutomatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AutomatBundle:Default:index.html.twig', array('name' => $name));
    }
}
